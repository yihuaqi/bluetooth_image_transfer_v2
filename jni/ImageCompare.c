#include "ImageCompare.h"
#include<android/log.h>
#include<android/bitmap.h>
#define DEBUG 1
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,"Native_Compare",__VA_ARGS__)

JNIEXPORT jboolean JNICALL Java_com_example_bluetooth_1image_1transfer_1v2_imagebuffer_ImageCompare_isSamePart
  (JNIEnv *env, jclass thiz, jobject wholeBitmap, jobject testBitmap, jint x, jint y){
	int offsetX = (int)(x);
	int offsetY = (int)(y);
	LOGD("Hello%d%d",offsetX,offsetY);
	AndroidBitmapInfo _wholeBitmapInfo;
	AndroidBitmapInfo _testBitmapInfo;
	if(AndroidBitmap_getInfo(env,wholeBitmap,&_wholeBitmapInfo)<0){
		LOGD("AndroidBitmap_getInfo failed for wholeBitmap");
		return JNI_FALSE;
	}
	if(AndroidBitmap_getInfo(env,testBitmap,&_testBitmapInfo)<0){
		LOGD("AndroidBitmap_getInfo failed for wholeBitmap");
		return JNI_FALSE;
	}
	if(DEBUG){
		//LOGD("WholeBitmap: Width:%d Height:%d",_wholeBitmapInfo);
	}
	return JNI_TRUE;

};

