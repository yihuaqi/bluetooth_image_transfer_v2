/*
 * Android Event Injector 
 *
 * Copyright (c) 2013 by Radu Motisan , radu.motisan@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 *
 */

#include <linux/input.h>

struct uinput_event {
	struct timeval time;
	uint16_t type;
	uint16_t code;
	int32_t value;
};

struct label {
    const char *name;
    int value;
};

#define LABEL(constant) { #constant, constant }
#define LABEL_END { NULL, -1 }



static struct label rel_labels[] = {
        LABEL(REL_X),
        LABEL(REL_Y),
        LABEL(REL_Z),
        LABEL(REL_RX),
        LABEL(REL_RY),
        LABEL(REL_RZ),
        LABEL(REL_HWHEEL),
        LABEL(REL_DIAL),
        LABEL(REL_WHEEL),
        LABEL(REL_MISC),
        LABEL_END,
};


static struct label msc_labels[] = {
        LABEL(MSC_SERIAL),
        LABEL(MSC_PULSELED),
        LABEL(MSC_GESTURE),
        LABEL(MSC_RAW),
        LABEL(MSC_SCAN),
        LABEL_END,
};

static struct label led_labels[] = {
        LABEL(LED_NUML),
        LABEL(LED_CAPSL),
        LABEL(LED_SCROLLL),
        LABEL(LED_COMPOSE),
        LABEL(LED_KANA),
        LABEL(LED_SLEEP),
        LABEL(LED_SUSPEND),
        LABEL(LED_MUTE),
        LABEL(LED_MISC),
        LABEL(LED_MAIL),
        LABEL(LED_CHARGING),
        LABEL_END,
};

static struct label rep_labels[] = {
        LABEL(REP_DELAY),
        LABEL(REP_PERIOD),
        LABEL_END,
};

static struct label snd_labels[] = {
        LABEL(SND_CLICK),
        LABEL(SND_BELL),
        LABEL(SND_TONE),
        LABEL_END,
};

static struct label id_labels[] = {
        LABEL(ID_BUS),
        LABEL(ID_VENDOR),
        LABEL(ID_PRODUCT),
        LABEL(ID_VERSION),
        LABEL_END,
};



static struct label ff_status_labels[] = {
        LABEL(FF_STATUS_STOPPED),
        LABEL(FF_STATUS_PLAYING),
        LABEL(FF_STATUS_MAX),
        LABEL_END,
};

static struct label ff_labels[] = {
        LABEL(FF_RUMBLE),
        LABEL(FF_PERIODIC),
        LABEL(FF_CONSTANT),
        LABEL(FF_SPRING),
        LABEL(FF_FRICTION),
        LABEL(FF_DAMPER),
        LABEL(FF_INERTIA),
        LABEL(FF_RAMP),
        LABEL(FF_SQUARE),
        LABEL(FF_TRIANGLE),
        LABEL(FF_SINE),
        LABEL(FF_SAW_UP),
        LABEL(FF_SAW_DOWN),
        LABEL(FF_CUSTOM),
        LABEL(FF_GAIN),
        LABEL(FF_AUTOCENTER),
        LABEL_END,
};

static struct label key_value_labels[] = {
        { "UP", 0 },
        { "DOWN", 1 },
        { "REPEAT", 2 },
        LABEL_END,
};
