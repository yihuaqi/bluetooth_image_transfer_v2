#include "ImageCompare.h"
#include<android/log.h>
#include<android/bitmap.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <jni.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <linux/kd.h>
#include <linux/fb.h>
#include <stddef.h>

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <asm/page.h>
#include "Screenshot.h"
#define DEBUG 1
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,"Native_Screenshot",__VA_ARGS__)

JNIEXPORT void JNICALL Java_com_example_bluetooth_1image_1transfer_1v2_screenshot_Screenshot_getScreenshot
  (JNIEnv *env, jclass clazz, jbyteArray jba, jintArray jia, jint ji){
	int index = (int) ji;
	int fd, ret, byte_per_frame;
	unsigned char	*framebuffer_memory;
	static struct fb_var_screeninfo vinfo;
	struct fb_fix_screeninfo finfo;
	signed char *baRegion;
	fd = open("/dev/graphics/fb0", O_RDONLY);
	if(fd < 0)
		{
			LOGD("======Cannot open /dev/graphics/fb0!");

		}

	ret = ioctl(fd, FBIOGET_VSCREENINFO, &vinfo);
		if(ret < 0 )
		{
			LOGD("======Cannot get variable screen information.");
			close(fd);

		}
	LOGD("Native called");
	LOGD("Offset:%d  -  %d",vinfo.xoffset,vinfo.yoffset);

	ret = ioctl(fd, FBIOGET_FSCREENINFO, &finfo);
	if(ret < 0 )
	{
		LOGD("Cannot get fixed screen information.");
		close(fd);

	}
	byte_per_frame = finfo.line_length * vinfo.yres;

	framebuffer_memory = (char *)mmap(
			0,
			byte_per_frame,
			PROT_READ,
			MAP_SHARED,
			fd,
			// Here should be the offset of the current frame.
			// But offsetX and offsetY doesn't work, so here is changed to the frame index
			finfo.line_length * 800 * index);

	baRegion = (char*)(*env)->GetByteArrayElements(env,jba,0);

	memcpy(baRegion,framebuffer_memory,byte_per_frame);

	if(framebuffer_memory == MAP_FAILED) {
		close(fd);
		LOGD("mmap for fb0 failed!");

	}





	close(fd);
};

