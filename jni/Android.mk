LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := EventInjector
LOCAL_SRC_FILES := EventInjector.c
LOCAL_LDLIBS := -llog 
 
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := ImageCompare
LOCAL_SRC_FILES := ImageCompare.c
LOCAL_LDLIBS := -llog -ljnigraphics
 
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := Screenshot
LOCAL_SRC_FILES := Screenshot.c
LOCAL_LDLIBS := -llog 
 
include $(BUILD_SHARED_LIBRARY)