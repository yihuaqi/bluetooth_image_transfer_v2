package com.example.bluetooth_image_transfer_v2.screenshot;

import java.io.IOException;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.example.bluetooth_image_transfer_v2.BluetoothImage;
import com.example.bluetooth_image_transfer_v2.Setting;
import com.example.bluetooth_image_transfer_v2.imagebuffer.ImageBuffer;
import com.example.bluetooth_image_transfer_v2.imagebuffer.ImageBufferFactory;
import com.example.bluetooth_image_transfer_v2.imagebuffer.SimpleImageBuffer;

/**
 * This is a Screenshot AsyncTask that continuously takes screenshots and
 * store into ImageBuffer.
 * @author YiH
 *
 */
public class FastScreenTask extends AsyncTask<Void, Void, Void>{
	long startTimeStamp = 0;
	long endTimeStamp = 0;
	
	// If it is not ScreenCasting then we don't take screenshot.
	private boolean isScreenCasting = false;
	private ImageBuffer mImageBuffer;
	Bitmap bm;
	@Override
	protected Void doInBackground(Void... arg0) {
		mImageBuffer = ImageBufferFactory.getBuffer();
		long start;
		long end;
		while(true){
			if(isScreenCasting){	
				startTimeStamp = System.currentTimeMillis();	
				try {
					
					bm = Screenshot.getScreenBitmap();
					Log.d("Time_Cost", "Screenshot,"+(System.currentTimeMillis()-startTimeStamp)+"");
					if(bm!=null){
						mImageBuffer.pushGridImage(bm);
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
			}
		}
	}
	public void startScreenCasting(){
		isScreenCasting = true;
	}
	public void stopScreenCasting(){
		isScreenCasting = false;
	}
	public boolean isScreenCasting(){
		return isScreenCasting;
	}
}

