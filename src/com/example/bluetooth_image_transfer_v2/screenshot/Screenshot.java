package com.example.bluetooth_image_transfer_v2.screenshot;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.bluetooth_image_transfer_v2.Setting;

/**
 * Takes screenshot by reading /dev/graphics/fb0 Average time is 100ms.
 * 
 * @author gudh
 * @data 2014-1-17
 */
public class Screenshot {

	final static String FB0FILE1 = "/dev/graphics/fb0";
	final static String FB0FILE2 = "/dev/fb0";

	static File fbFile;
	static FileInputStream graphics = null;
	static public int screenWidth = 480; // �?幕宽（�?素，如：480px）
	static public int screenHeight = 800; // �?幕高（�?素，如：800p）
	static byte[] piex;
	static public boolean hasInitialized = false;
	static private int frameIndex = 0;
	/**
	 * On Samsung Galaxy S3 mini there are three buffer in FrameBuffer.
	 */
	static public final int FRAME_NUMBER = 3;
	static {

		System.loadLibrary("Screenshot");

	}

	public static void nextIndex() {
		frameIndex = (frameIndex + 1) % FRAME_NUMBER;
		Log.d("FrameBuffer Index", "Current Index:" + frameIndex);
	}

	/**
	 * init
	 * 
	 * @param context
	 */

	public static void init(DisplayMetrics displayMetrics) {
		fbFile = new File(FB0FILE1);
		if (!fbFile.exists()) {
			File nFile = new File(FB0FILE2);
			if (nFile.exists()) {
				fbFile = nFile;
			}
		}
		// Get r/w privilege
		try {
			Process sh = Runtime.getRuntime().exec("su", null, null);
			OutputStream os = sh.getOutputStream();
			os.write(("chmod 777 " + fbFile.getAbsolutePath()).getBytes());
			os.flush();
			os.close();
			sh.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}

		DisplayMetrics dm = displayMetrics;

		screenWidth = dm.widthPixels; // Screen Width
		screenHeight = dm.heightPixels; // Screen Height

		PixelFormat pixelFormat = new PixelFormat();
		PixelFormat.getPixelFormatInfo(PixelFormat.RGBA_8888, pixelFormat);
		int deepth = pixelFormat.bytesPerPixel;
		piex = new byte[screenHeight * screenWidth * deepth * FRAME_NUMBER];
		hasInitialized = true;
	}

	/**
	 * 测试截图
	 */
	@SuppressLint("SdCardPath")
	public static void testShot() {
		long start = System.currentTimeMillis();
		try {

			// Log.d("fast_screen", "Got screenbitmap");
			Bitmap bm = getScreenBitmap();
			saveMyBitmap(bm, Environment.getExternalStorageDirectory()
					.getAbsolutePath()
					+ "/"
					+ System.currentTimeMillis()
					+ ".jpg");

		} catch (IOException e) {
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		// Log.i("Screenshot", "time cost:" + (end - start));
	}

	/**
	 * Save Bitmap to files
	 * 
	 * @param bitmap
	 * @param bitName
	 * @throws IOException
	 */
	public static void saveMyBitmap(Bitmap bitmap, String bitName)
			throws IOException {
		File f = new File(bitName);
		f.createNewFile();
		FileOutputStream fOut = new FileOutputStream(f);

		bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
		fOut.flush();
		fOut.close();
	}

	/**
	 * Call init() first before getScreenBitmap()
	 * 
	 * @return
	 * @throws IOException
	 */
	public synchronized static Bitmap getScreenBitmap() throws IOException {
		try {
			graphics = new FileInputStream(fbFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		DataInputStream dStream = new DataInputStream(graphics);
		int[] colors = new int[screenHeight * screenWidth];
		switch (Setting.screenshot_method) {
		case Setting.SCREENSHOT_JNI:
			getScreenshot(piex, colors, frameIndex);

			for (int m = 0; m < colors.length; m++) {
				int b = (piex[m * 4] & 0xFF);
				int g = (piex[m * 4 + 1] & 0xFF);
				int r = (piex[m * 4 + 2] & 0xFF);
				int a = (piex[m * 4 + 3] | 0xFF);
				colors[m] = (a << 24) + (r << 16) + (g << 8) + b;

			}
			break;
		case Setting.SCREENSHOT_READ_FB0:
			try {
				dStream.readFully(piex);
				dStream.close();

				for (int m = 0; m < colors.length; m++) {
					int b = (piex[(m + colors.length * frameIndex) * 4] & 0xFF);
					int g = (piex[(m + colors.length * frameIndex) * 4 + 1] & 0xFF);
					int r = (piex[(m + colors.length * frameIndex) * 4 + 2] & 0xFF);
					int a = (piex[(m + colors.length * frameIndex) * 4 + 3] | 0xFF);
					colors[m] = (a << 24) + (r << 16) + (g << 8) + b;
				}
			} catch (IOException e) {

				/*
				 * Can't read dev/graphics/fb0.4 In this case we call
				 * "/system/bin/screencap -p" and read the bitmap back from
				 * sdcard
				 */

				// return callScreenCap();
				callScreenCap();
				Log.d("Redirect_STDOUT", "Screenshot!");
				for (int m = 0; m < colors.length; m++) {
					int r = (piex[m * 4] & 0xFF);
					int g = (piex[m * 4 + 1] & 0xFF);
					int b = (piex[m * 4 + 2] & 0xFF);
					int a = (piex[m * 4 + 3] | 0xFF);
					colors[m] = (a << 24) + (r << 16) + (g << 8) + b;
				}
				// JniClient.getScreenPixels(colors);
				// Log.d("Native_test", "Changed:"+colors[0]);

			}
			break;
		case Setting.SCREENSHOT_SCREENCAP:
			callScreenCap();
			Log.d("Redirect_STDOUT", "Screenshot!");
			for (int m = 0; m < colors.length; m++) {
				int r = (piex[m * 4] & 0xFF);
				int g = (piex[m * 4 + 1] & 0xFF);
				int b = (piex[m * 4 + 2] & 0xFF);
				int a = (piex[m * 4 + 3] | 0xFF);
				colors[m] = (a << 24) + (r << 16) + (g << 8) + b;
			}
			break;

		default:
			break;
		}

		Log.d("Landscape_Framebuffer", screenWidth + ":" + screenHeight);

		if (Setting.IS_LANDSCAPE) {
			Bitmap result = Bitmap.createBitmap(colors, screenHeight,
					screenWidth, Bitmap.Config.ARGB_8888);
			return result;
		} else {
			Bitmap result = Bitmap.createBitmap(colors, screenWidth,
					screenHeight, Bitmap.Config.ARGB_8888);
			return result;
		}

	}

	private static Bitmap callScreenCap() {

		Bitmap bm;
		String filePathName = takeScreenShot();

		if (filePathName != null) {
			bm = BitmapFactory.decodeFile(filePathName);
			Log.i("slow_screen", "Slow but still got screenshot.");
			return bm;
		}
		return null;

	}

	private static String takeScreenShot() {
		File ssDir = new File(Environment.getExternalStorageDirectory(),
				"/screenshots");
		if (ssDir.exists() == false) {
			Log.i("slow_screen",
					"Screenshot directory doesn't already exist, creating...");
			if (ssDir.mkdirs() == false) {
				// TODO: We're kinda screwed... what can be done?
				Log.w("slow_screen",
						"Failed to create directory structure necessary to work with screenshots!");
				return null;
			}
		}
		File ss = new File(ssDir, "ss.raw");
		if (ss.exists() == true) {
			ss.delete();
			Log.i("slow_screen", "Deleted old Screenshot file.");
		}

		// String cmd = "/system/bin/screencap -p "+ ss.getAbsolutePath();
		String cmd = "/system/bin/screencap";
		runSuShellCommand(cmd);
		return ss.getAbsolutePath();

	}

	private static void runSuShellCommand(String cmd) {
		Log.d("Redirect_STDOUT", "Using command:" + cmd);
		Runtime runtime = Runtime.getRuntime();
		Process proc = null;
		OutputStreamWriter osw = null;
		StringBuilder sbstdOut = new StringBuilder();
		StringBuilder sbstdErr = new StringBuilder();

		try { // Run Script
			proc = runtime.exec("su");

			osw = new OutputStreamWriter(proc.getOutputStream());
			osw.write(cmd);
			osw.flush();
			Log.d("Redirect_STDOUT", "Flushing command:" + cmd);
			osw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
			Log.d("Redirect_STDOUT", "IOException!");
		} finally {
			if (osw != null) {
				try {
					osw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		try {
			if (proc != null) {
				Log.d("Redirect_STDOUT", "Wait for");
				DataInputStream dis = new DataInputStream(proc.getInputStream());
				try {
					dis.readFully(piex);
					Log.d("Redirect_STDOUT", "Readfully");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				proc.waitFor();

			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			Log.d("Redirect_STDOUT", "InterruptedException");
		}

	}

	private static Object readBufferedReader(InputStreamReader inputStreamReader) {

		BufferedReader reader = new BufferedReader(inputStreamReader);
		StringBuilder found = new StringBuilder();
		String currLine = null;
		String sep = System.getProperty("line.separator");
		try {
			// Read it all in, line by line.
			while ((currLine = reader.readLine()) != null) {
				// Log.d("Redirect_STDOUT", currLine);
				found.append(currLine);
				found.append(sep);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return found;

	}

	public static native void getScreenshot(byte[] ba, int[] ia, int index);
}