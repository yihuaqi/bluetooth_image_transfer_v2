package com.example.bluetooth_image_transfer_v2.imagebuffer;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;

import com.example.bluetooth_image_transfer_v2.Setting;

/**
 * This is the container that holds the Bitmap and various information.
 * 
 * @author YiH
 * 
 */
public class GridImage {

	/**
	 * A copy of byte[] from Bitmap.getPixels();
	 */
	byte[] image;

	/**
	 * Grid index in Fixed Window Protocol
	 */
	int index;

	/**
	 * Dimensions of the bitmap
	 */
	int height;
	int width;
	Bitmap bitmap;

	/**
	 * Start point of the bitmap in Flexible Window Protocol
	 */
	int x;
	int y;

	/**
	 * Constructor for Fixed Window Protocol.
	 * 
	 * @param b
	 *            Bitmap
	 * @param i
	 *            grid index
	 */
	public GridImage(Bitmap b, int i) {
		height = b.getHeight();
		width = b.getWidth();
		bitmap = b;
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		b.compress(Setting.COMPRESS_FORMAT, Setting.COMPRESS_QUALITY, bytes);

		image = bytes.toByteArray();
		index = i;
	}

	/**
	 * Constructor for Flexible Window Protocol
	 * 
	 * @param b
	 *            Bitmap
	 * @param x2
	 *            x coordinate of start point
	 * @param y2
	 *            y coordinate of start point
	 */
	public GridImage(Bitmap b, int x2, int y2) {
		height = b.getHeight();
		width = b.getWidth();
		bitmap = b;
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		x = x2;
		y = y2;
		b.compress(Setting.COMPRESS_FORMAT, Setting.COMPRESS_QUALITY, bytes);
		image = bytes.toByteArray();

	}

	public int getIndex() {
		return index;

	}

	public byte[] getBytes() {
		return image;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
