package com.example.bluetooth_image_transfer_v2.imagebuffer;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * This is the implementation of a Simple ImageBuffer that only store and 
 * transfer the whole screenshot.
 * @author YiH
 *
 */
public class SimpleImageBuffer implements ImageBuffer{
	GridImage gi;
	static SimpleImageBuffer instance;
	private SimpleImageBuffer() {
	}
	
	/**
	 * Same as pushGridImage, just the index is 0.
	 * @param b
	 * @param index
	 */
	public void pushGridImage(Bitmap b, int index) {
		Log.d("ImageBuffer", "Image Pushed");
		synchronized (this) {
			gi = new GridImage(b, index);
		}
	}

	@Override
	public GridImage getImage() {
		
		if(gi!=null){
			GridImage result = gi;
			gi = null;
			return result;
		
		}
		return null;
		
	}

	/**
	 * Get the singleton class
	 * @return
	 */
	static public ImageBuffer getInstance() {
		if(instance==null){
			instance = new SimpleImageBuffer();
			setDimentions(1, 1);
		}
		return instance;
	}
	
	
	static public void setDimentions(int col, int row) {
		return;
		
	}

	@Override
	public void pushGridImage(Bitmap b) {
		pushGridImage(b,0);
		
	}

	@Override
	public void receiveFocus(int index, boolean focused) {
		return;
		
	}

	@Override
	public void receiveWindow(int x, int y, int width, int height) {
		return;
		
	}

}
