package com.example.bluetooth_image_transfer_v2.imagebuffer;

import android.graphics.Bitmap;

public interface ImageBuffer {
	/**
	 * Push a Bitmap to the ImageBuffer
	 * @param b the Bitmap is to store.
	 */
	void pushGridImage(Bitmap b);
	
	/**
	 * Pop a GridImage from the ImageBuffer
	 * @return a GridImage
	 */
	GridImage getImage();
	
	/**
	 * Handle Focus message
	 * @param index the index of grid
	 * @param focused true if grid gains focus, otherwise false.
	 */
	void receiveFocus(int index, boolean focused);
	
	/**
	 * Handle Window message
	 * @param x x coordinate of start point
	 * @param y y coordinate of start point
	 * @param width width of window
	 * @param height height of window
	 */
	void receiveWindow(int x, int y, int width, int height);
}
