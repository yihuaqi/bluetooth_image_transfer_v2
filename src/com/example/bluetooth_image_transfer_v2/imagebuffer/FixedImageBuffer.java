package com.example.bluetooth_image_transfer_v2.imagebuffer;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.bluetooth_image_transfer_v2.Setting;

/**
 * This class implements ImageBuffer of Fixed Window Protocol.
 * 
 * @author YiH
 * 
 */
public class FixedImageBuffer implements ImageBuffer {

	/**
	 * Singleton instance of this class
	 */
	static ImageBuffer instance;

	static GridImage[] mGridImages;

	/**
	 * Indicates which grids are focused.
	 */
	static boolean focused[];

	/**
	 * Indicates which grids have not been sent.
	 */
	static boolean isNewImage[];

	/**
	 * Number of grids.
	 */
	static int gridNumber;

	/**
	 * Number of grids columns
	 */
	static int cols;

	/**
	 * Number of grids rows
	 */
	static int rows;

	/**
	 * Indicates if the Whole Image has been sent
	 */
	static boolean isNewWholeImage = true;

	/**
	 * The Whole Image
	 */
	static GridImage mWholeImage;

	/**
	 * Index of which GridImage is going to be sent.
	 */
	int currentIndex = 0;

	@Override
	public void pushGridImage(Bitmap b) {
		int bHeight = b.getHeight();
		int bWidth = b.getWidth();
		int gridHeight = bHeight / rows;
		int gridWidth = bWidth / cols;
		if (areFocusedGridImagesChanged(b)) {

			// In the case that focused grid images changed, we send all focused
			// gridImages.
			for (int r = 0; r < rows; r++) {
				for (int c = 0; c < cols; c++) {
					int index = c + cols * r;
					if (focused[index]) {
						Bitmap tempBitmap = Bitmap.createBitmap(b, c
								* gridWidth, r * gridHeight, gridWidth,
								gridHeight);
						mGridImages[index] = new GridImage(tempBitmap, index);
						isNewImage[index] = true;
					}
				}
			}
		} else {
			if (isWholeImageChanged(b)) {
				// In the case that focused grid images didn't change but the
				// whole
				// Image Changed. We send the whole image.
				// If nothing changed then we do nothing.
				mWholeImage = new GridImage(b, -1);
				isNewWholeImage = true;
			}
		}
	}

	/**
	 * Given a screenshot, check if it is different from the last sent
	 * screenshot.
	 * 
	 * @param b
	 *            Bitmap of Screenshot
	 * @return true if it is different, otherwise false.
	 */
	private boolean isWholeImageChanged(Bitmap b) {
		if (mWholeImage == null) {

			return true;
		} else {
			Bitmap wholeBitmap = mWholeImage.getBitmap();
			boolean isSame = ImageCompare.isSameWhole(b, wholeBitmap);

			return !isSame;
		}
	}

	/**
	 * Check if any focused GridImage is different from the corresponding part
	 * in the given screenshot.
	 * 
	 * @param b
	 *            Screenshot bitmap.
	 * @return true if any of the GridImage is different.
	 */
	private boolean areFocusedGridImagesChanged(Bitmap b) {
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				int index = c + cols * r;
				if (focused[index]) {
					if (isGridImageChanged(b, r, c)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Check if the GridImage with given index is different from the
	 * corresponding part in the given screenshot
	 * 
	 * @param b
	 *            Screenshot bitmap.
	 * @param row
	 *            true if it is different.
	 * @param col
	 * @return
	 */
	private boolean isGridImageChanged(Bitmap b, int row, int col) {
		int index = col + cols * row;
		if (mGridImages[index] == null) {
			return true;
		} else {

			int gridWidth = b.getWidth() / cols;
			int gridHeight = b.getHeight() / rows;
			Bitmap testBitmap = mGridImages[index].getBitmap();
			int offsetX = col * gridWidth;
			int offsetY = row * gridHeight;
			boolean isSame = ImageCompare.isSamePart(b, testBitmap, offsetX,
					offsetY);
			return !isSame;
		}
	}

	@Override
	public GridImage getImage() {

		if (isNewWholeImage && allSent()) {
			System.currentTimeMillis();
			GridImage result = mWholeImage;
			isNewWholeImage = false;
			Log.d("screen_change", "Get whole image");
			return result;
		} else {

			while (true) {
				if (focused[currentIndex] && mGridImages[currentIndex] != null
						&& isNewImage[currentIndex]) {
					GridImage result = mGridImages[currentIndex];
					isNewImage[currentIndex] = false;
					currentIndex = (currentIndex + 1) % gridNumber;
					Log.d("screen_change", "Get Grid Image");
					return result;
				}
				currentIndex = (currentIndex + 1) % gridNumber;
			}
		}
	}

	/**
	 * 
	 * @return true if all gridImages are sent, otherwise false;
	 */
	private boolean allSent() {
		for (int i = 0; i < gridNumber; i++) {
			if (isNewImage[i] && focused[i]) {
				return false;
			}
		}
		return true;

	}

	/**
	 * Get a instance of this ImageBuffer.
	 * 
	 * @return a FixedImageBuffer
	 */
	static public ImageBuffer getInstance() {
		if (instance == null) {
			instance = new FixedImageBuffer();
			setDimentions(Setting.GRID_COLS, Setting.GRID_ROWS);
		}
		return instance;
	}

	/**
	 * Set the Dimentions of grids.
	 * 
	 * @param c
	 *            number of columns
	 * @param r
	 *            number of rows
	 */
	static public void setDimentions(int c, int r) {
		cols = c;
		rows = r;
		gridNumber = cols * rows;
		mGridImages = new GridImage[gridNumber];
		focused = new boolean[gridNumber];
		isNewImage = new boolean[gridNumber];
		for (int i = 0; i < gridNumber; i++) {
			focused[i] = true;
			isNewImage[i] = true;
		}
		return;
	}

	@Override
	public void receiveFocus(int i, boolean f) {
		focused[i] = f;
		Log.d("FOCUS_TEST", "Grid#" + i + " changed " + f);
	}

	@Override
	public void receiveWindow(int x, int y, int width, int height) {
		return;

	}

}
