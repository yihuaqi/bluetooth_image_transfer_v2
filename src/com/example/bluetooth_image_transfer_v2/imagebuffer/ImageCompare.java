package com.example.bluetooth_image_transfer_v2.imagebuffer;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Utils class for doing ImageCompare.
 * @author YiH
 *
 */
public class ImageCompare {
	static{
		System.loadLibrary("ImageCompare");
	}
	
	/**
	 * Check if the test Bitmap is different from whole Bitmap start at (x,y)
	 * Note that for performance consideration, when the area of test Bitmap is
	 * not one fourth of the whole Bitmap, it will run isSameWhole 
	 * @param whole Whole Screenshot Bitmap
	 * @param test test Bitmap
	 * @param x x coordinate of start point
	 * @param y y coordiante of start point
	 * @return
	 */
	static boolean isSamePart(Bitmap whole, Bitmap test, int x, int y){
		int testHeight = test.getHeight();
		int testWidth = test.getWidth();
		
		if(y + testHeight <= whole.getHeight() && x + testWidth <= whole.getWidth()){	
			Bitmap partBitmap = Bitmap.createBitmap(whole, x, y,testWidth,testHeight);
			return partBitmap.sameAs(test);
		} else {
			return false;
		}
		
		
		
		
	}
	
	/**
	 * Check if two Bitmap are the same.
	 * @param whole
	 * @param test
	 * @return
	 */
	static boolean isSameWhole(Bitmap whole, Bitmap test){
		return whole.sameAs(test);
	}
	
}
