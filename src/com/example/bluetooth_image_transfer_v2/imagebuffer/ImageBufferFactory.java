package com.example.bluetooth_image_transfer_v2.imagebuffer;

import com.example.bluetooth_image_transfer_v2.Setting;

/**
 * Factory class for get ImageBuffer. Types are defined in Setting.PROTOCOL
 * @author YiH
 *
 */
public class ImageBufferFactory {

	public static ImageBuffer getBuffer(){
		return produce(Setting.PROTOCOL);
	}
	
	private static ImageBuffer produce(int type){
		if(type==Setting.PROTOCOL_SIMPLE){
			return SimpleImageBuffer.getInstance();
		}
		if(type==Setting.PROTOCOL_FIXED){
			return FixedImageBuffer.getInstance();
		}
		if(type==Setting.PROTOCOL_FLEXIBLE){
			return FlexibleImageBuffer.getInstance();
		}
		
		return null;
	}
}
