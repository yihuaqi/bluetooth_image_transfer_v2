package com.example.bluetooth_image_transfer_v2.imagebuffer;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.bluetooth_image_transfer_v2.Setting;
import com.example.bluetooth_image_transfer_v2.screenshot.Screenshot;

/**
 * This class implements ImageBuffer of Flexible Window Protocol
 * 
 * @author YiH
 * 
 */
public class FlexibleImageBuffer implements ImageBuffer {

	/**
	 * Singleton instance of this class
	 */
	static ImageBuffer instance;

	static GridImage mGridImage;

	/**
	 * Indicates if the GridImage has been sent
	 */
	static boolean isNewImage;

	/**
	 * Indicates if the WholeImage has been sent
	 */
	static boolean isNewWholeImage = true;

	//
	private int x = 0;
	private int y = 0;
	private int width = Screenshot.screenWidth;
	private int height = Screenshot.screenHeight;
	static GridImage mWholeImage;

	/**
	 * Indicate whether we can update window information
	 */
	boolean canReceiveWindow = true;

	@Override
	public void pushGridImage(Bitmap b) {

		// We don't want window to be changed when we try to crop bitmap.
		// Even though this is not thread-safe, it is much faster than the
		// thread
		// safe solution.
		canReceiveWindow = false;
		if (!isFlexibleImageSame(b) || !Setting.ALLOW_SENDING_WHOLE) {

			// If the gridImage within the window changed, we will send the
			// gridImage first.
			int gridHeight = height;
			int gridWidth = width;
			long startTimeStamp = System.currentTimeMillis();
			if (x + gridWidth > b.getWidth() || y + gridHeight > b.getHeight()) {
				Log.d("Landscape_Exception", b.getWidth() + "  " + x + "+"
						+ gridWidth);
			} else {
				Bitmap tempBitmap = Bitmap.createBitmap(b, x, y, gridWidth,
						gridHeight);
				mGridImage = new GridImage(tempBitmap, x, y);
				Log.d("Landscape_Update", "Push grid image!");
				Log.d("Time_Cost", "Compressing,"
						+ (System.currentTimeMillis() - startTimeStamp));
				isNewImage = true;
			}

		} else {
			if (!isWholeImageSame(b)) {
				Log.d("Landscape_Update", "Push whole image!");
				// If the gridImage didn't change but the whole changed, we
				// send the whole screenshot.
				mWholeImage = new GridImage(b, 0, 0);
				isNewWholeImage = true;
			}
		}
		canReceiveWindow = true;
	}

	/**
	 * Check if the GridImage has changed from the corresponding part in
	 * screenshot
	 * 
	 * @param b
	 *            Screenshot bitmap
	 * @return true if nothing changed within the GridImage.
	 */
	private boolean isFlexibleImageSame(Bitmap b) {
		if (mGridImage == null) {
			return false;
		} else {
			Bitmap testBitmap = mGridImage.getBitmap();
			int offsetX = mGridImage.getX();
			int offsetY = mGridImage.getY();

			long start = System.currentTimeMillis();

			start = System.currentTimeMillis();
			int testHeight = testBitmap.getHeight();
			int testWidth = testBitmap.getWidth();
			int wholeHeight = b.getHeight();
			int wholeWidth = b.getWidth();
			boolean isSame;
			if (wholeHeight * wholeWidth / (testHeight * testWidth) > 4) {
				isSame = ImageCompare.isSamePart(b, testBitmap, offsetX,
						offsetY);
			} else {
				if (mWholeImage == null) {
					return false;
				}
				isSame = ImageCompare.isSameWhole(b, mWholeImage.getBitmap());
			}
			long end = System.currentTimeMillis();
			if (!isSame) {
				Log.d("Bitmap_Compare", "Flexible is not the same");
			}

			return isSame;
			// return false;
		}

	}

	/**
	 * Check if the Screenshot is the same with the last one we have sent
	 * 
	 * @param b
	 *            Screenshot bitmap
	 * @return true if they are the same.
	 */
	private boolean isWholeImageSame(Bitmap b) {
		if (mWholeImage == null) {

			return false;
		} else {
			Bitmap wholeBitmap = mWholeImage.getBitmap();

			long start = System.currentTimeMillis();

			boolean isSame = ImageCompare.isSameWhole(b, wholeBitmap);
			long end = System.currentTimeMillis();
			if (!isSame) {
				Log.d("Bitmap_Compare", "Whole is not the same");
			}
			return isSame;
		}
	}

	@Override
	public GridImage getImage() {

		if (isNewWholeImage && !isNewImage) {
			GridImage result = mWholeImage;
			isNewWholeImage = false;
			Log.d("screen_change", "Get whole image");
			return result;
		} else {

			if (mGridImage != null && isNewImage) {
				GridImage result = mGridImage;
				isNewImage = false;

				Log.d("screen_change", "Get Grid Image");
				return result;
			}
		}
		return null;
	}

	/**
	 * 
	 * @return get the singleton instance of this class.
	 */
	static public ImageBuffer getInstance() {
		if (instance == null) {
			instance = new FlexibleImageBuffer();

		}
		return instance;
	}

	@Override
	public void receiveFocus(int i, boolean f) {
		return;
	}

	@Override
	public void receiveWindow(int x, int y, int width, int height) {

		if (canReceiveWindow) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			Log.d("Out_of_boundary", "Receive window:" + this.width + "X"
					+ this.height);
		}

	}

}
