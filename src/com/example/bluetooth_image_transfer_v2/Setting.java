package com.example.bluetooth_image_transfer_v2;

import android.graphics.Bitmap;

import com.example.bluetooth_image_transfer_v2.imagebuffer.FixedImageBuffer;
import com.example.bluetooth_image_transfer_v2.imagebuffer.FlexibleImageBuffer;
import com.example.bluetooth_image_transfer_v2.imagebuffer.SimpleImageBuffer;
import com.example.bluetooth_image_transfer_v2.screenshotsender.FixedScreenshotSender;
import com.example.bluetooth_image_transfer_v2.screenshotsender.FlexibleScreenshotSender;
import com.example.bluetooth_image_transfer_v2.screenshotsender.SimpleScreeshotSender;

/**
 * Store all constants and settings.
 * 
 * @author YiH
 * 
 */
public class Setting {

	/**
	 * Used to control the screencapture frame rate.
	 */
	public static final int CAPTURE_SLEEP_TIME = 0;

	/**
	 * The Compress format used to compress the screenshot JPEG is much faster
	 * than PNG.
	 */
	public static final Bitmap.CompressFormat COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;

	/**
	 * The Compress quality for JPEG format.
	 */
	public static final int COMPRESS_QUALITY = 75;

	/**
	 * Grid parameter for Fixed Grid protocal.
	 */
	public static final int GRID_ROWS = 4;
	public static final int GRID_COLS = 4;

	/**
	 * @see SimpleScreeshotSender
	 * @see SimpleImageBuffer
	 */
	public static final int PROTOCOL_SIMPLE = 0;
	/**
	 * @see FixedScreenshotSender
	 * @see FixedImageBuffer
	 */
	public static final int PROTOCOL_FIXED = 1;
	/**
	 * @see FlexibleScreenshotSender
	 * @see FlexibleImageBuffer
	 */
	public static final int PROTOCOL_FLEXIBLE = 2;
	/**
	 * Transmitting protocol.
	 * 
	 * @see #PROTOCOL_FIXED
	 * @see #PROTOCOL_FLEXIBLE
	 * @see #PROTOCOL_SIMPLE
	 */
	public static final int PROTOCOL = PROTOCOL_FLEXIBLE;

	/**
	 * True means it allows injecting single touch event from the Glass.
	 */
	public static final boolean ALLOW_CLICK = true;

	/**
	 * True means the Flexible protoco will send the whole sceenshot when the
	 * current viewing part does not change. False then it will always send the
	 * part of viewing window.
	 * 
	 */
	public static final boolean ALLOW_SENDING_WHOLE = true;

	/**
	 * Encode the screenshot using Landscape mode if true.
	 */
	public static boolean IS_LANDSCAPE = false;

	/**
	 * Taking screenshot by reading the Framebuffer in Java. Only works on some
	 * devices.
	 */
	public static final int SCREENSHOT_READ_FB0 = 0;
	/**
	 * Taking screenshot by calling screencap. Works on most devices but is
	 * slower than other method.
	 */
	public static final int SCREENSHOT_SCREENCAP = 1;
	/**
	 * Taking screenshot by reading the Framebuffer in JNI. Some devices can
	 * also get the offset of the current current framebuffer. But not on Galaxy
	 * S3 mini.
	 */
	public static final int SCREENSHOT_JNI = 2;

	public static int screenshot_method = SCREENSHOT_READ_FB0;
}
