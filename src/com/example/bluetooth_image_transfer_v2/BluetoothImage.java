package com.example.bluetooth_image_transfer_v2;

import com.example.bluetooth_image_transfer_v2.ScreenCaptureService.ScreenCaptureServiceBinder;
import com.example.bluetooth_image_transfer_v2.bluetooth.BluetoothService;
import com.example.bluetooth_image_transfer_v2.eventinjector.Events;
import com.example.bluetooth_image_transfer_v2.screenshot.Screenshot;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is the main activity that displays the whole interface.
 * @author YiH
 *
 */
public class BluetoothImage extends Activity {
	
	/** Message types sent from the BluetoothService handler */
	public static final int MESSAGE_IS_RECEIVING = 0;	
	public static final int MESSAGE_IS_SENDING = 1;		
	public static final int MESSAGE_HAS_SENT = 2;		
	public static final int MESSAGE_HAS_RECEIVED = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_CLICK = 5;
	
	/** Debugging */
    public static final String TAG = "image transfer";
    
    /** Member object for controling the input events */
    public static Events mEvents;
    public static Events.InputDevice mTouchScreenDevice;
    
	/** Key names received from the BluetoothService handler */
    public static final String DEVICE_NAME = "device_name";

    /** Layout views */
    Button mStartButton;				//Start or stop screencast
    TextView mStatusTextView;			//Show status text
    private Button btn_Click;			//Test button for controlling input events
    private Button btn_Test;			//Button that counts time of being clicked
    
    
    /** Member object for the BluetoothService */
	private BluetoothService mBluetoothService;
    
	/** Member object for the ScreenCaputreService */
    private ScreenCaptureService mScreenCaptureService;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		// Find the touchscreen device 
		if (Setting.ALLOW_CLICK){
			mEvents = new Events();
			mEvents.Init();
			for (int i = 0; i < mEvents.m_Devs.size(); i++){
				mEvents.m_Devs.get(i).Open(true);
				Log.d("Native_test", mEvents.m_Devs.get(i).getName()+"");
				if (mEvents.m_Devs.get(i).getName().contains("touchscreen")){
					mTouchScreenDevice = mEvents.m_Devs.get(i);
				}
			}
		}
		
		// Keep the screen on 
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		if(getResources().getConfiguration().orientation==getResources().getConfiguration().ORIENTATION_LANDSCAPE){
			Setting.IS_LANDSCAPE = true;
		}
		setContentView(R.layout.activity_main);
		BluetoothService.syncInit(mHandler);
		setupTest();
		
		// Start and bind the ScreenCaptureService.
		Intent mIntent = new Intent(this,ScreenCaptureService.class);
		bindService(mIntent, mConnection, Context.BIND_AUTO_CREATE);

	}
	
	private ServiceConnection mConnection = new ServiceConnection() {
		
		@Override
		public void onServiceDisconnected(ComponentName name) {}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			ScreenCaptureServiceBinder mBinder = (ScreenCaptureServiceBinder) service;
			mScreenCaptureService = mBinder.getService();

		}
	};

	/**
	 * Initialize layout views and other constants.
	 */
	private void setupTest() {
		Log.d("fast screen", "setup test");
		
		// Initialize the start button with an OnClickListener
		mStartButton = (Button) findViewById(R.id.main_start_btn);
		mStartButton.setOnClickListener(mOnClickListener);
		
		mStatusTextView = (TextView) findViewById(R.id.main_status_txt);
		
		// Initialize test button if clicking is allowed.
		btn_Click = (Button) findViewById(R.id.main_click);
		btn_Test = (Button) findViewById(R.id.main_test);
		if (Setting.ALLOW_CLICK){
			btn_Click.setOnClickListener(mTestOnClickListener);
			btn_Test.setOnClickListener(mTestOnClickListener);
		} else {
			btn_Click.setVisibility(View.GONE);
			btn_Test.setVisibility(View.GONE);
		}
	}
	
	protected void onDestroy(){
		super.onDestroy();
		Log.d("fast_screen", "Stop Service");
		
		// Stop the ScreenCapture.
		unbindService(mConnection);
	}

	/**
	 * OnClickListener used by testing controling inputevents
	 */
	OnClickListener mTestOnClickListener = new OnClickListener() {
		int count = 0;
		boolean down=true;
		@Override
		public void onClick(View v) {
			if (v == btn_Click){
				Log.d("Native_test","Test:"+ btn_Test.getX()+":"+btn_Test.getY());
				Log.d("Native_test", "Click:"+btn_Click.getX()+":"+btn_Click.getY());
				Log.d("Native_test", "Start:"+mStartButton.getX()+":"+mStartButton.getY());
				Log.d("Native_test", "Screen:"+Screenshot.screenWidth+":"+Screenshot.screenHeight);
				
				// Inject a TouchDown event on Test Button.
				mTouchScreenDevice.SendTouchDownAbs(450, 770);
			}
			if (v == btn_Test){
				count++;
				btn_Test.setText(count+"");
			}
		}
	};
	
	/**
	 * Handles Click Event on Start Button.
	 */
	OnClickListener mOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			try{
				if (v == mStartButton){
					if (mScreenCaptureService.isScreenCasting()){
						
						// If ScreenCast is on, then turn it off.
						mScreenCaptureService.stopScreenCast();
						mStartButton.setText("Start Screencast");
					} else {
						
						//If ScreenCast is off, then turn it on.
						mScreenCaptureService.startScreenCast();
						mStartButton.setText("Stop Screencast");
					}
				}
			} catch (NullPointerException e){
				e.printStackTrace();
				Toast.makeText(BluetoothImage.this, "Please check your bluetooth connection",Toast.LENGTH_LONG).show();
				
			}
		}
	};

	/**
	 * Handles message received from BluetoothService.
	 */
	private Handler mHandler = new Handler(){
		
		@Override
		public void handleMessage(Message msg){
			switch (msg.what){
			case MESSAGE_IS_RECEIVING:
				mStatusTextView.setText("Total: "+msg.arg1+" Receiving...");
				break;
			case MESSAGE_IS_SENDING:
				mStatusTextView.setText("Total: "+msg.arg1+" Sending...");
				break;
			case MESSAGE_HAS_SENT:
				mStatusTextView.setText("All sent: "+msg.arg1);
				break;
			case MESSAGE_HAS_RECEIVED:
				mStatusTextView.setText("All received: "+msg.arg2);
				break;
			case MESSAGE_DEVICE_NAME:
				mStatusTextView.setText("Connected to "+msg.getData().getString(DEVICE_NAME));
				break;
			case MESSAGE_CLICK:
				mStatusTextView.setText("Clicked on ("+msg.arg1+","+msg.arg2+")");
				Log.d("CLICK_TEST", "Clicked on ("+msg.arg1+","+msg.arg2+")");
				break;
			case 999:
				mStatusTextView.setText("Test: "+msg.getData().getString("text"));
				break;
			}
			super.handleMessage(msg);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
