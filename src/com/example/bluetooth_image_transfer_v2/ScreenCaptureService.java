package com.example.bluetooth_image_transfer_v2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import com.example.bluetooth_image_transfer_v2.bluetooth.BluetoothService;
import com.example.bluetooth_image_transfer_v2.imagebuffer.ImageBufferFactory;


import com.example.bluetooth_image_transfer_v2.screenshot.FastScreenTask;
import com.example.bluetooth_image_transfer_v2.screenshot.Screenshot;
import com.example.bluetooth_image_transfer_v2.screenshotsender.ScreenSendTask;
import com.example.bluetooth_image_transfer_v2.screenshotsender.ScreenshotSender;
import com.example.bluetooth_image_transfer_v2.screenshotsender.ScreenshotSenderFactory;



import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.SettingInjectorService;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

/**
 * This service handles turning on and off the ScreenCast, connecting to
 * glass,
 * @author YiH
 *
 */
public class ScreenCaptureService extends Service{
	
	private BluetoothAdapter mBluetoothAdapter = null;
	private BluetoothService mBluetoothService = null;
	
	private ScreenSendTask mScreenSendTask;
	private FastScreenTask mScreencapTask;
	private final IBinder mBinder = new ScreenCaptureServiceBinder();
	
	public class ScreenCaptureServiceBinder extends Binder{
		public ScreenCaptureService getService(){
			return ScreenCaptureService.this;
		}
	}
	
	@Override
	public void onCreate() {		
		
		// Create a bluetooth socket to the glass.
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null){
			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_SHORT).show();
			stopSelf();
			return;
		}
		if (!mBluetoothAdapter.isEnabled()){
			Toast.makeText(this, "Please pair your devices first", Toast.LENGTH_SHORT).show();
		} else {
			if (mBluetoothService == null){
				mBluetoothService = BluetoothService.getInstance();
				Log.d("back_ground", "Initialize new BluetoothImageService");
			}
		}
		// Get a device with name containing "Glass" from bonded devices.
		Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
		Iterator<BluetoothDevice> it = devices.iterator();
		BluetoothDevice device = null;
		BluetoothDevice temp;
		while (it.hasNext()){
			temp = it.next();
			Log.d("Bluetooth_Device", temp.getName());
			 if(temp.getName().contains("Glass")){
				 Log.d("Bluetooth_Device", "Contains:"+temp.getName());
				 device = temp;
			 }
		}
		
		if (device == null){
			Toast.makeText(this, "No Glass is paired", Toast.LENGTH_SHORT).show();
		} else {

			mBluetoothService.connect(device, true);
			if(!Screenshot.hasInitialized){
				DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
				Screenshot.init(metrics);
				
			}
			
			// Start Screencap asynTask and ScreenSend asynTask.
			mScreencapTask = new FastScreenTask();
			mScreencapTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			mScreenSendTask = new ScreenSendTask("simple");
			mScreenSendTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			
		}
		super.onCreate();
	}
	
	/**
	 * Start ScreenCast
	 */
	public void startScreenCast(){
		mScreencapTask.startScreenCasting();
		mScreenSendTask.startScreenCasting();
	}
	
	/**
	 * Stop ScreenCast
	 */
	public void stopScreenCast(){
		mScreencapTask.stopScreenCasting();
		mScreenSendTask.stopScreenCasting();
	}
	
	/**
	 * 
	 * @return true if any of the ScreenCap or ScreenSend asynTask is running.
	 */
	public boolean isScreenCasting(){
		return mScreencapTask.isScreenCasting()||mScreenSendTask.isScreenCasting();
	}



	@Override
	public void onDestroy() {
		
		mBluetoothService.stop();
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}
	
	

}
