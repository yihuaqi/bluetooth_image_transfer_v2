package com.example.bluetooth_image_transfer_v2.screenshotsender;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.example.bluetooth_image_transfer_v2.BluetoothImage;
import com.example.bluetooth_image_transfer_v2.Setting;
import com.example.bluetooth_image_transfer_v2.bluetooth.BluetoothService;
import com.example.bluetooth_image_transfer_v2.imagebuffer.GridImage;

import android.graphics.Bitmap;
import android.util.Log;

public class SimpleScreeshotSender implements ScreenshotSender{
	BluetoothService mBluetoothService;
	// Config options for decoding and compressing.
		
	@Override
	public void init(BluetoothService bs,String buffertype) {
		mBluetoothService = bs;
		
	}

	@Override
	public void sendGridImage(GridImage gi) {

		
		//Compressing into PNG format takes a lot of time
		//Using JEPG will be much faster.					
		

		byte[] image = gi.getBytes();		
		
		mBluetoothService.writeSimpleImage(image);
		
	}

	
	public void sendInit(int width, int height) {
		mBluetoothService.writeSimpleInit(width,height);
		
	}

	

	@Override
	public void sendSimpleInit(int width, int height) {
		mBluetoothService.writeSimpleInit(width,height);
		
	}

	@Override
	public void sendFixedInit(int width, int height, int rows, int cols) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendFlexibleInit(int width, int height) {
		// TODO Auto-generated method stub
		
	}

}
