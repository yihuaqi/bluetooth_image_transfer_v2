package com.example.bluetooth_image_transfer_v2.screenshotsender;

import com.example.bluetooth_image_transfer_v2.bluetooth.BluetoothService;
import com.example.bluetooth_image_transfer_v2.imagebuffer.GridImage;

/**
 * ScreenshotSender implementation for Fixed Window Protocol
 * @author YiH
 *
 */
public class FixedScreenshotSender implements ScreenshotSender{
	BluetoothService mBluetoothService;
	@Override
	public void init(BluetoothService bs,String buffertype) {
		mBluetoothService = bs;
		
	}

	@Override
	public void sendGridImage(GridImage gi) {
		byte[] image = gi.getBytes();		
		mBluetoothService.writeFixedImage(image,gi.getIndex());	
	}

	@Override
	public void sendSimpleInit(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendFixedInit(int width, int height, int rows, int cols) {
		mBluetoothService.writeFixedInit(width,height,rows,cols);
		
	}

	@Override
	public void sendFlexibleInit(int width, int height) {
		// TODO Auto-generated method stub
		
	}

}
