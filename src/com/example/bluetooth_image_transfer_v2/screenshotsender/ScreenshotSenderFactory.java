package com.example.bluetooth_image_transfer_v2.screenshotsender;

import com.example.bluetooth_image_transfer_v2.Setting;

/**
 * ScreenshotSender Factory class that produce different ScreenshotSender
 * @author YiH
 *
 */
public class ScreenshotSenderFactory {
	
	static public ScreenshotSender getInstance(){
		return produce(Setting.PROTOCOL);
	}
	static private ScreenshotSender produce(int type){
		switch (type) {
		case Setting.PROTOCOL_SIMPLE:
			return new SimpleScreeshotSender();
			
		case Setting.PROTOCOL_FIXED:
			return new FixedScreenshotSender();
			
		case Setting.PROTOCOL_FLEXIBLE:
			return new FlexibleScreenshotSender();
		default:
			break;
		}
		return null;
	}
}
