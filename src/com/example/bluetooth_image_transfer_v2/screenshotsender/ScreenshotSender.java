package com.example.bluetooth_image_transfer_v2.screenshotsender;

import com.example.bluetooth_image_transfer_v2.bluetooth.BluetoothService;
import com.example.bluetooth_image_transfer_v2.imagebuffer.GridImage;

/**
 * This interface handles sending init information and gridimage to glass.
 * 
 * @author YiH
 * 
 */
public interface ScreenshotSender {

	/**
	 * Should be called before any communication.
	 * 
	 * @param bs
	 *            BluetoothService instance.
	 * @param bufferType
	 */
	public void init(BluetoothService bs, String bufferType);

	/**
	 * Send a GridImage
	 * 
	 * @param gi
	 */
	public void sendGridImage(GridImage gi);

	/**
	 * Send init information in Simple Protocol
	 * 
	 * @param width
	 *            ScreenWidth
	 * @param height
	 *            ScreenHeight
	 */
	public void sendSimpleInit(int width, int height);

	/**
	 * Send init information in Fixed Window Protocol
	 * 
	 * @param width
	 *            ScreenWidth
	 * @param height
	 *            ScreenHeight
	 * @param rows
	 *            Number of grid rows
	 * @param cols
	 *            Number of grid columns
	 */
	public void sendFixedInit(int width, int height, int rows, int cols);

	/**
	 * Send init information in Flexible Window Protocol
	 * 
	 * @param width
	 *            ScreenWidth
	 * @param height
	 *            ScreenHeight
	 */
	public void sendFlexibleInit(int width, int height);

}
