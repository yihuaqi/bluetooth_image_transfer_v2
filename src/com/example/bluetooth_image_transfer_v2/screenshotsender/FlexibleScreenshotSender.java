package com.example.bluetooth_image_transfer_v2.screenshotsender;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.example.bluetooth_image_transfer_v2.BluetoothImage;
import com.example.bluetooth_image_transfer_v2.Setting;
import com.example.bluetooth_image_transfer_v2.bluetooth.BluetoothService;
import com.example.bluetooth_image_transfer_v2.imagebuffer.GridImage;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * ScreenshotSender implementation for Flexbible Window Protocol
 * @author YiH
 *
 */
public class FlexibleScreenshotSender implements ScreenshotSender{
	BluetoothService mBluetoothService;
	// Config options for decoding and compressing.
		
	@Override
	public void init(BluetoothService bs,String buffertype) {
		mBluetoothService = bs;
		
	}

	@Override
	public void sendGridImage(GridImage gi) {
		byte[] image = gi.getBytes();	
		mBluetoothService.writeFlexibleImage(image,gi.getX(),gi.getY());
	}
	
	@Override
	public void sendSimpleInit(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendFixedInit(int width, int height, int rows, int cols) {
		
		
	}

	@Override
	public void sendFlexibleInit(int width, int height) {
		mBluetoothService.writeFlexibleInit(width,height);
		
	}

}
