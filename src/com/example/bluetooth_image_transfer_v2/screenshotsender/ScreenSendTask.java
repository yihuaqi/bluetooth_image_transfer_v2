package com.example.bluetooth_image_transfer_v2.screenshotsender;



import android.os.AsyncTask;
import com.example.bluetooth_image_transfer_v2.Setting;
import com.example.bluetooth_image_transfer_v2.bluetooth.BluetoothService;
import com.example.bluetooth_image_transfer_v2.imagebuffer.GridImage;
import com.example.bluetooth_image_transfer_v2.imagebuffer.ImageBuffer;
import com.example.bluetooth_image_transfer_v2.imagebuffer.ImageBufferFactory;
import com.example.bluetooth_image_transfer_v2.screenshot.Screenshot;

/**
 * AsyncTask for sending screenshot
 * @author YiH
 *
 */
public class ScreenSendTask extends AsyncTask<Void, Void, Void>{
	long startTimeStamp = 0;
	long endTimeStamp = 0;
	private boolean isScreenCasting = false;
	private ScreenshotSender mScreenshotSender;
	private ImageBuffer mImageBuffer;
	private boolean hasInitilized =false;
	GridImage gi;
	public ScreenSendTask(String senderType) {
		
		// ScreenshotSender and ImageBuffer are of the same protocol
		mScreenshotSender = ScreenshotSenderFactory.getInstance();
		mImageBuffer = ImageBufferFactory.getBuffer();
		mScreenshotSender.init(BluetoothService.getInstance(),"");
	}
	@Override
	protected Void doInBackground(Void... arg0) {
		long start;
		long end;
		
		while(true){
			if(isScreenCasting){	
			startTimeStamp = System.currentTimeMillis();	
			start = System.currentTimeMillis();
			
			gi=mImageBuffer.getImage();
			if(gi!=null){
				if(!hasInitilized){
					// First time communication will send init information to glass.
					switch (Setting.PROTOCOL) {
					case Setting.PROTOCOL_SIMPLE:
						mScreenshotSender.sendSimpleInit(Screenshot.screenWidth,Screenshot.screenHeight);
						break;
					case Setting.PROTOCOL_FIXED:
						mScreenshotSender.sendFixedInit(Screenshot.screenWidth,Screenshot.screenHeight,Setting.GRID_ROWS,Setting.GRID_COLS);
						break;
					case Setting.PROTOCOL_FLEXIBLE:
						mScreenshotSender.sendFlexibleInit(Screenshot.screenWidth,Screenshot.screenHeight);
						break;
					default:
						break;
					}
					hasInitilized = true;
				}
				mScreenshotSender.sendGridImage(gi);
			}
			long timeCost = System.currentTimeMillis()-startTimeStamp;
			if(timeCost<Setting.CAPTURE_SLEEP_TIME){
				try {
					Thread.sleep(Setting.CAPTURE_SLEEP_TIME-timeCost);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			
			}
		}
	}
	public void startScreenCasting(){
		isScreenCasting = true;
		hasInitilized = false;
	}
	public void stopScreenCasting(){
		isScreenCasting = false;
	}
	public boolean isScreenCasting(){
		return isScreenCasting;
	}
}

