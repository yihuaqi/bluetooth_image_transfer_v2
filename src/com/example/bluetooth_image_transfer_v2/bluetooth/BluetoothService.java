package com.example.bluetooth_image_transfer_v2.bluetooth;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.bluetooth_image_transfer_v2.BluetoothImage;
import com.example.bluetooth_image_transfer_v2.Setting;
import com.example.bluetooth_image_transfer_v2.imagebuffer.GridImage;
import com.example.bluetooth_image_transfer_v2.imagebuffer.ImageBuffer;
import com.example.bluetooth_image_transfer_v2.imagebuffer.ImageBufferFactory;
import com.example.bluetooth_image_transfer_v2.screenshot.Screenshot;

/**
 * This class handles bluetooth communication.
 * 
 * @author YiH
 * 
 */
public class BluetoothService {

	private final BluetoothAdapter mAdapter;
	private Handler mHandler;
	private AcceptThread mAcceptThread;

	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;

	private int mState;
	// Name for the SDP record when creating server socket
	private static final String NAME_SECURE = "BluetoothTestSecure";
	private static final String NAME_INSECURE = "BluetoothTestInsecure";

	// Unique UUID for this application
	private static final UUID MY_UUID_SECURE = UUID
			.fromString("fa87c0d0-ffff-11de-8a39-0800200c9a67");
	private static final UUID MY_UUID_INSECURE = UUID
			.fromString("8ce255c0-eeee-11e0-ac64-0800200c9a67");

	// Types of Connection state
	public static final int STATE_NONE = 0; // we're doing nothing
	public static final int STATE_LISTEN = 1; // now listening for incoming
												// connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing
													// connection
	public static final int STATE_CONNECTED = 3; // now connected to a remote
													// device
	public static final String TAG = "BS tag";

	// Singleton of this class.
	private static BluetoothService instance = null;

	/**
	 * Initialize the BluetoothService in a singleton way.
	 * 
	 * @param handler
	 */
	private BluetoothService() {
		mAdapter = BluetoothAdapter.getDefaultAdapter();

	}

	/**
	 * Initialize the BluetoothService with given handler.
	 * 
	 * @param handler
	 */
	public static synchronized void syncInit(Handler handler) {
		if (instance == null) {
			instance = new BluetoothService();
			instance.setHandler(handler);
		}
	}

	public void setHandler(Handler handler) {
		mHandler = handler;
	}

	/**
	 * Return a BluetoothService singleton. If syncInit is not called before.
	 * Then a BluetoothService singleton without handler will be created.
	 * 
	 * @return
	 */
	public static BluetoothService getInstance() {
		if (instance == null) {
			syncInit(null);
		}
		return instance;
	}

	/**
	 * Set the current state of the bluetooth connection
	 * 
	 * @param state
	 *            An integer defining the current connection state.
	 */
	private void setState(int state) {
		Log.d(TAG, "setState()" + mState + "->" + state);
		mState = state;
	}

	/**
	 * Start the bluetooth connection. Specifically start AcceptThread to begin
	 * a session in listening (server) mode. Called by
	 */
	public void start() {
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		setState(STATE_LISTEN);

		if (mAcceptThread == null) {
			mAcceptThread = new AcceptThread(true);
			mAcceptThread.start();
		}

	}

	/**
	 * Start the ConnectThread to initiate a connection to a remote device.
	 * 
	 * @param device
	 *            The BluetoothDevice to connect
	 * @param secure
	 *            Socket Security type - Secure (true), Insecure (false)
	 */
	public void connect(BluetoothDevice device, boolean secure) {

		// Cancel any thread attempting to make a connection
		if (mState == STATE_CONNECTING) {
			if (mConnectedThread != null) {
				mConnectThread.cancle();
				mConnectThread = null;
			}
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		// Start the thread to connect with the given device
		mConnectThread = new ConnectThread(device, secure);
		mConnectThread.start();
		setState(STATE_CONNECTING);

	}

	/**
	 * Start the ConnectedThread to begin managing a Bluetooth connection
	 * 
	 * @param socket
	 *            The BluetoothSocket on which the connection was made
	 * @param device
	 *            The bluetoothDevice that has been connected
	 * @param socketType
	 *            The socket type.
	 */
	public void connected(BluetoothSocket socket, BluetoothDevice device,
			final String socketType) {

		// Cancel the thread that completed the connection
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		// Cancel the accept thread because we only want to connect to one
		// device
		if (mAcceptThread != null) {
			mAcceptThread.cancle();
			mAcceptThread = null;
		}

		// Start the thread to manage the connection and perform trasmission
		mConnectedThread = new ConnectedThread(socket, socketType);
		mConnectedThread.start();

		// Send the name of the connected device back to the UI activity
		if (mHandler != null) {
			Message msg = mHandler
					.obtainMessage(BluetoothImage.MESSAGE_DEVICE_NAME);
			Bundle bundle = new Bundle();
			bundle.putString(BluetoothImage.DEVICE_NAME, device.getName());
			msg.setData(bundle);
			mHandler.sendMessage(msg);
		}
		setState(STATE_CONNECTED);
	}

	/**
	 * Stop all threads
	 */
	public void stop() {
		if (mConnectThread != null) {
			mConnectThread.cancle();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancle();
			mConnectedThread = null;
		}

		if (mAcceptThread != null) {
			mAcceptThread.cancle();
			mAcceptThread = null;
		}
		setState(STATE_NONE);
	}

	/**
	 * Indicate that the connection attempt failed and notify the UI Activity
	 */
	private void connectionFailed() {
		Log.d(TAG, "Connection Failed");

		// Start the service over to restart listening mode
		BluetoothService.this.start();
	}

	private void connectionLost() {
		Log.d(TAG, "Connection Lost");

		// Start the service over to restart listening mode
		BluetoothService.this.start();
	}

	/**
	 * This thread runs while listening for incoming connnections. It behave
	 * like a server-side client. It runs until a connection is accepted (or
	 * until cancelled).
	 * 
	 * @author YiH
	 * 
	 */
	class AcceptThread extends Thread {

		// The local server socket
		final BluetoothServerSocket mmServerSocket;
		String mSocketType;

		public AcceptThread(boolean secure) {
			BluetoothServerSocket tmp = null;
			mSocketType = secure ? "Secure" : "Insecure";

			// Create a new listening server socket
			try {
				if (secure) {
					tmp = mAdapter.listenUsingRfcommWithServiceRecord(
							NAME_SECURE, MY_UUID_SECURE);
				} else {
					tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(
							NAME_INSECURE, MY_UUID_INSECURE);
				}

			} catch (IOException e) {
				Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
			}
			mmServerSocket = tmp;
		}

		public void run() {
			setName("AcceptThread" + mSocketType);
			BluetoothSocket socket = null;

			// Listen to the server socket if we're not connected
			while (mState != STATE_CONNECTED) {
				try {
					/*
					 * This is a blocking call and will only return on a
					 * successfull connection or an exception
					 */
					socket = mmServerSocket.accept();
				} catch (IOException e) {
					Log.e(TAG, "Socket Type: " + mSocketType
							+ "accept() failed", e);
					break;
				}

				// If a connection was accepted
				if (socket != null) {
					synchronized (BluetoothService.this) {
						switch (mState) {
						case STATE_LISTEN:
						case STATE_CONNECTING:
							// Situation normal. Start the connected thread.
							connected(socket, socket.getRemoteDevice(),
									mSocketType);
							break;
						case STATE_NONE:
						case STATE_CONNECTED:
							// Either not ready or already connected. Terminate
							// new socket.
							try {
								socket.close();
							} catch (IOException e) {
								Log.e(TAG, "Could not close unwanted socket", e);
							}
							break;
						}
					}
				}
			}
		}

		public void cancle() {
			try {
				mmServerSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "Socket Type" + mSocketType
						+ "close() of server failed", e);
			}
		}
	}

	/**
	 * This thread runs while attempting to make an outgoing connection with a
	 * device. It runs straight through; the connection either succeeds or fails
	 * 
	 * @author YiH
	 * 
	 */
	class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;
		private String mSocketType;

		public ConnectThread(BluetoothDevice device, boolean secure) {
			mmDevice = device;
			BluetoothSocket tmp = null;
			mSocketType = secure ? "Secure" : "Insecure";

			// Get a BluetoothSocket for a connection with the given
			// BluetoothDevice

			try {
				if (secure) {
					tmp = device
							.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
				} else {
					tmp = device
							.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE);
				}
			} catch (IOException e) {
				Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
			}
			mmSocket = tmp;
		}

		public void run() {
			setName("ConnectThread" + mSocketType);

			// Always cancel discovery because it will slow down a connection
			mAdapter.cancelDiscovery();

			// Make a connection to the BluetoothSocket
			try {
				// This is a blocking call and will only return on a
				// successful connection or an exception
				mmSocket.connect();
			} catch (IOException e) {
				// Close the socket
				try {
					mmSocket.close();
				} catch (IOException e2) {
					Log.e(TAG, "unable to close() " + mSocketType
							+ " socket during connection failure", e2);
				}
				connectionFailed();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (BluetoothService.this) {
				mConnectThread = null;

			}

			// Start the connected thread
			connected(mmSocket, mmDevice, mSocketType);
		}

		public void cancle() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect " + mSocketType
						+ " socket failed", e);
			}
		}

	}

	/**
	 * This thread runs during a connection with a remote device It handles all
	 * incoming and outgoing transmissions.
	 * 
	 * @author YiH
	 * 
	 */
	class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		private final DataOutputStream dos;
		private final DataInputStream dis;

		// Types of status.
		private final int WAITING = 0;
		private final int RECEIVING_GRID = 1;
		private final int RECEIVING_WINDOW = 2;
		private final int RECEIVING_CLICK = 3;
		int state;
		char stateChar;
		ImageBuffer mImageBuffer;

		public ConnectedThread(BluetoothSocket socket, String socketType) {
			Log.d(TAG, "create ConnectedThread: " + socketType);
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			// Get the BluetoothSocket input and output streams
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
			dos = new DataOutputStream(mmOutStream);
			dis = new DataInputStream(mmInStream);
			mImageBuffer = ImageBufferFactory.getBuffer();
		}

		public void run() {

			// Keep listening to the DataInputStream while connected
			while (true) {
				try {
					// Read from the DataInputStream
					switch (state) {
					case WAITING:
						stateChar = dis.readChar();
						if (stateChar == 'g') {
							// We are receiving Grid focus changes in Fixed Grid
							// Protocol
							state = RECEIVING_GRID;
						}
						if (stateChar == 'w') {
							// We are receiving Window Size in Flexible Grid
							// Protocol
							state = RECEIVING_WINDOW;
						}
						if (stateChar == 'c') {
							// We are receiving Click event.
							state = RECEIVING_CLICK;
						}
						if (stateChar == 'o') {
							Log.d("Bluetooth_Landscape", "Orientation Changed!");
							Setting.IS_LANDSCAPE = !Setting.IS_LANDSCAPE;
						}
						if (stateChar == 'f') {
							Screenshot.nextIndex();
						}
						break;
					case RECEIVING_GRID:
						// Receive the index of grid that has changed
						int index = dis.readInt();
						// Receive whether the grid lose focus or gain focus
						boolean focused = dis.readBoolean();
						// Update in the ImageBuffer
						mImageBuffer.receiveFocus(index, focused);
						state = WAITING;
						break;
					case RECEIVING_WINDOW:
						// The offset x coordinate of window
						int x = dis.readInt();
						// The offset y coordinate of window
						int y = dis.readInt();
						// The width of window
						int width = dis.readInt();
						// The height of window
						int height = dis.readInt();
						// Update in the ImageBuffer
						mImageBuffer.receiveWindow(x, y, width, height);
						state = WAITING;
						break;
					case RECEIVING_CLICK:
						// The cursor x coordinate
						int cursor_x = (int) dis.readFloat();
						// The cursor y coordinate
						int cursor_y = (int) dis.readFloat();
						if (mHandler != null) {
							Message msg = mHandler.obtainMessage(
									BluetoothImage.MESSAGE_CLICK,
									(int) cursor_x, (int) cursor_y);
							mHandler.sendMessage(msg);
						}
						if (Setting.ALLOW_CLICK) {
							// Inject touchdown event.
							if (!Setting.IS_LANDSCAPE) {
								BluetoothImage.mTouchScreenDevice
										.SendTouchDownAbs(cursor_x, cursor_y);
							} else {
								BluetoothImage.mTouchScreenDevice
										.SendTouchDownAbs(480 - cursor_y,
												cursor_x);
								Log.d("Landscape_TouchEvent", 480 - cursor_y
										+ ":" + cursor_x);
							}
						}
						state = WAITING;
						break;
					default:
						break;
					}

				} catch (IOException e) {
					Log.e(TAG, "disconnected", e);
					connectionLost();
					// Start the service over to restart listening mode
					BluetoothService.this.start();
					break;
				}

			}

		}

		/**
		 * Send a byte array and index of the {@link GridImage} to the
		 * BluetoothDevice
		 * 
		 * @param buffer
		 *            byte[] from {@link Bitmap#getPixels}
		 * @param index
		 *            The index the {@link GridImage}
		 */
		public void writeFixedImage(byte[] buffer, int index) {
			try {
				dos.writeChar('g');
				dos.writeInt(buffer.length);
				dos.write(buffer);
				dos.writeInt(index);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void cancle() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}

		}

		/**
		 * Send the Init message of Simple Protocol
		 * 
		 * @param width
		 *            Screen width
		 * @param height
		 *            Screen height
		 */
		public void writeSimpleInit(int width, int height) {
			try {
				dos.writeChar('i');
				dos.writeInt(Setting.PROTOCOL_SIMPLE);
				dos.writeInt(width);
				dos.writeInt(height);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		/**
		 * Send the Init message of Fixed Window Protocol
		 * 
		 * @param width
		 *            ScreenWidth
		 * @param height
		 *            ScreenHeight
		 * @param rows
		 *            Number of grid rows
		 * @param cols
		 *            Number of grid columns
		 */
		public void writeFixedInit(int width, int height, int rows, int cols) {
			try {
				dos.writeChar('i');
				dos.writeInt(Setting.PROTOCOL_FIXED);
				dos.writeInt(width);
				dos.writeInt(height);
				Log.d("FOCUS_TEST", "!!!!!!!!!Width and height!!!!!!!!!"
						+ width + " " + height);
				dos.writeInt(rows);
				dos.writeInt(cols);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		/**
		 * Send the Init message of Flexible Window Protocol.
		 * 
		 * @param width
		 *            Screen Width
		 * @param height
		 *            Screen Height
		 */
		public void writeFlexibleInit(int width, int height) {
			try {
				dos.writeChar('i');
				dos.writeInt(Setting.PROTOCOL_FLEXIBLE);
				dos.writeInt(width);
				dos.writeInt(height);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		/**
		 * Send the byte[] of a Bitmap to the BluetoothDevice
		 * 
		 * @param buffer
		 *            byte[] from {@link Bitmap#getPixels()}
		 */
		public void writeSimpleImage(byte[] buffer) {
			try {
				dos.writeChar('g');
				dos.writeInt(buffer.length);
				dos.write(buffer);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		/**
		 * Send the byte[] of a Bitmap, the start point of the Bitmap to the
		 * BluetoothDevice
		 * 
		 * @param buffer
		 *            byte[] from {@link Bitmap#getPixels()}
		 * @param x
		 *            x coordinate of start point
		 * @param y
		 *            y coordinate of start point
		 */
		public void writeFlexibleImage(byte[] buffer, int x, int y) {
			try {
				dos.writeChar('g');
				dos.writeInt(buffer.length);
				dos.write(buffer);
				dos.writeInt(x);
				dos.writeInt(y);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	/**
	 * Send the byte[] of a Bitmap with its index in Fixed Window Protocol
	 * 
	 * @param image
	 *            byte[] from {@link Bitmap#getPixels()}
	 * @param index
	 *            the index of the {@link GridImage}.
	 */
	public void writeFixedImage(byte[] image, int index) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}

		r.writeFixedImage(image, index);

		Log.d("Write_logic", "-------------------------");
	}

	/**
	 * Send the Init message of Simple Protocol
	 * 
	 * @param width
	 *            Screen Width
	 * @param height
	 *            Screen Height
	 */
	public void writeSimpleInit(int width, int height) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeSimpleInit(width, height);

	}

	/**
	 * Send the Init message of Fixed Window Protocol
	 * 
	 * @param width
	 *            Screen Width
	 * @param height
	 *            Screen Height
	 * @param rows
	 *            Number of grid rows
	 * @param cols
	 *            Number of grid cols
	 */
	public void writeFixedInit(int width, int height, int rows, int cols) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeFixedInit(width, height, rows, cols);

	}

	/**
	 * Send the Init message of Flexible Window Protocol
	 * 
	 * @param width
	 *            Screen Width
	 * @param height
	 *            Screen Height
	 */
	public void writeFlexibleInit(int width, int height) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeFlexibleInit(width, height);

	}

	/**
	 * Send the byte[] of a {@link Bitmap} in Simple Protocol
	 * 
	 * @param image
	 *            byte[] from {@link Bitmap#getPixels()}
	 */
	public void writeSimpleImage(byte[] image) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeSimpleImage(image);

	}

	/**
	 * Send the byte[] of a {@link Bitmap} in Flexible Window Protocol
	 * 
	 * @param image
	 *            byte[] from {@link Bitmap#getPixels()}
	 * @param x
	 *            x coordinate of start point
	 * @param y
	 *            y coordinate of start point
	 */
	public void writeFlexibleImage(byte[] image, int x, int y) {
		ConnectedThread r;
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;

		}
		r.writeFlexibleImage(image, x, y);

	}

}
